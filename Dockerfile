FROM nginx:stable-alpine
COPY web /usr/share/nginx/html/web/
ADD index.html /usr/share/nginx/html/
ADD manifest.json /usr/share/nginx/html/
ADD sw.js /usr/share/nginx/html/